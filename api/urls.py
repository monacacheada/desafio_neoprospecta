from django.urls import path
from . import views


app_name = 'api'
urlpatterns = [
    path('profile/<int:id>', views.api_profile, name='api_profile'),
]
