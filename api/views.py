from auth_desafio.models import Profile
from django.forms.models import model_to_dict
from django.http import JsonResponse
from django.shortcuts import get_object_or_404


def api_profile(request, id):
    profile = get_object_or_404(Profile, pk=id, publico=True)
    data = model_to_dict(profile)
    return JsonResponse(data)
