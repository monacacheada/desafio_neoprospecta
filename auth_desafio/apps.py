from django.apps import AppConfig


class AuthDesafioConfig(AppConfig):
    name = 'auth_desafio'
