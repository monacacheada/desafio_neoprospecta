from django.urls import path
from . import views


app_name = 'auth_desafio'
urlpatterns = [
    path('', views.home, name='home'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('register', views.register, name='register'),
    path('profile/<int:id>/edit', views.profile_edit, name='profile_edit'),
    path('profile/<int:id>/public', views.profile_public, name='profile_public'),
    path('profile/<int:id>/setup', views.profile_setup, name='profile_setup'),
]
