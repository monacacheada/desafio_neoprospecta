from .forms import RegisterForm, ProfileForm, PublicProfileForm, UserLoginForm
from .models import Profile, User
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render, redirect


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('auth:profile_edit', id=user.profile.id)
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form': form})


def initial_checks(request):
    if not request.user.profile:
        profile = Profile()
        profile.user = request.user
        profile.save()
        request.user.profile = profile
        request.user.save()


@login_required
def profile_edit(request, id):
    initial_checks(request)
    profile = Profile.objects.filter(pk=id, user=request.user).first()
    if not profile:
        raise Http404
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            profile = form.save()
            profile.user = request.user
            profile.save()
            request.user.first_access = False
            request.user.save()
            return redirect(
                'auth:profile_public', id=profile.id
            )
    else:
        form = ProfileForm(instance=profile)
    return render(request, 'profile_edit.html', {'form': form})


@login_required
def profile_public(request, id):
    initial_checks(request)
    if request.user.first_access:
        return redirect('auth:profile_edit', id=request.user.profile.id)
    profile = Profile.objects.filter(pk=id).first()
    if not profile:
        raise Http404
    is_user_profile = profile == request.user.profile
    if not is_user_profile and not profile.publico:
        raise Http404
    return render(
        request, 'profile_public.html', {
            'is_user_profile': is_user_profile, 'profile': profile
        }
    )


@login_required
def profile_setup(request, id):
    initial_checks(request)
    if request.user.first_access:
        return redirect('auth:profile_edit', id=request.user.profile.id)
    profile = Profile.objects.filter(pk=id, user=request.user).first()
    if not profile:
        raise Http404
    if request.method == 'POST':
        form = PublicProfileForm(request.POST)
        if form.is_valid():
            publico = form.cleaned_data.get('publico', False)
            request.user.profile.publico = publico
            request.user.profile.save()
            return redirect(
                'auth:profile_public', id=request.user.profile.id
            )
    else:
        form = PublicProfileForm(instance=profile)
    return render(request, 'profile_setup.html', {'form': form})


@login_required
def home(request):
    initial_checks(request)
    if request.user.first_access:
        return redirect('auth:profile_edit', id=request.user.profile.id)
    return redirect('auth:profile_public', id=request.user.profile.id)


def logout_view(request):
    logout(request)
    return redirect('auth:login')


def login_view(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email', '')
            user = User.objects.filter(email=email).first()
            if user:
                login(request, user)
                return redirect('auth:home')
            form.add_error('email', 'Este email não existe')
    else:
        form = UserLoginForm()
    return render(request, 'login.html', {'form': form})
