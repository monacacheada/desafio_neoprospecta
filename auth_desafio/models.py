from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Profile(models.Model):
    nome = models.CharField(max_length=254, null=True, blank=True)
    idade = models.IntegerField(null=True, blank=True)
    sexo = models.CharField(max_length=254, null=True, blank=True)
    pais = models.CharField(max_length=254, null=True, blank=True)
    estado = models.CharField(max_length=254, null=True, blank=True)
    cidade = models.CharField(max_length=254, null=True, blank=True)
    filme = models.CharField(max_length=254, null=True, blank=True)
    publico = models.BooleanField(default=False)


class User(AbstractUser):
    """User model."""

    username = None
    email = models.EmailField(_('email address'), unique=True)
    first_access = models.BooleanField(default=True)
    profile = models.OneToOneField(
        Profile, on_delete=models.CASCADE, null=True, blank=True
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()


@receiver(pre_save, sender=User)
def create_user_candidato(sender, instance, **kwargs):
    if not instance.profile:
        profile = Profile()
        profile.user = instance
        profile.save()
        instance.profile = profile
