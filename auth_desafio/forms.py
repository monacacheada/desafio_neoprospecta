from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import User, Profile


class RegisterForm(UserCreationForm):
    email = forms.EmailField(max_length=254)

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2')


class UserLoginForm(forms.Form):
    email = forms.CharField(
        required=True,
        label='Email'
    )
    password = forms.CharField(
        required=True,
        label='Password',
        max_length=32,
        widget=forms.PasswordInput()
    )


class ProfileForm(forms.ModelForm):
    nome = forms.CharField(max_length=254)
    idade = forms.IntegerField()
    sexo = forms.CharField(max_length=254)
    pais = forms.CharField(max_length=254)
    estado = forms.CharField(max_length=254)
    cidade = forms.CharField(max_length=254)
    filme = forms.CharField(max_length=254)

    class Meta:
        model = Profile
        fields = ('nome', 'idade', 'sexo', 'pais', 'estado', 'cidade', 'filme')


class PublicProfileForm(forms.ModelForm):
    publico = forms.BooleanField(required=False)

    class Meta:
        model = Profile
        fields = ('publico', )
